const getItemHtml = async (item, needle) => {
    var img = item.img != null ? item.img : `https://sg-storage-staging-v1.s3.amazonaws.com/posters/dc10a5a3-4b79-4c40-9668-3d9596eb3421.jpg`;
    var eventType = `<div class='dg_widget_result_type'>${item.type === 'transcription' ? 'Video' : 'Page'}</div>`;
    var itemContent = item.content;
    var needleSpaced = needle.split('+').join(' ');
    itemContent = itemContent.replace(needleSpaced, `<b style='background: #ff0;'>${needleSpaced}</b>`);
    var content = item.type === 'transcription' ? `<div class='dg_widget_result_item_content'><i class="fa fa-comments" style='margin-right: 6px;'></i> <span>${itemContent}</span></div>`:``;
    var temp = `
    <div class='dg_widget_result_item'>
        <div class='dg_widget_result_item_details'>
            <a class='dg_widget_result_item_link' href='${item.url}'>
                ${eventType} 
                <div style='width:100%'><div class='dg_widget_result_item_title'>${item.event}</div>${content}</div>
            </a>
        </div>
    </div>`;
    return temp;
}

export default getItemHtml;