const hideResults = () => {
    var resultContainer = document.getElementById('dg_widget_result_container');
    resultContainer.style.display = 'none';
}

const showResults = () => {
    var resultContainer = document.getElementById('dg_widget_result_container');
    console.log(resultContainer.childNodes);
    if (resultContainer.childNodes.length) {
        resultContainer.style.display = 'flex';
    }
}

const clearResults = () => {
    var resultContainer = document.getElementById('dg_widget_result_container');
    var oldResults = resultContainer.querySelectorAll('.dg_widget_result_item');
    oldResults.forEach(item => item.remove());
}

const showEmptyText = () => {
    var resultContainer = document.getElementById('dg_widget_result_container');
    var emptyText = document.getElementById('dg_widget_result_noResult');
    emptyText.style.display = 'block';
    if (resultContainer.childNodes.length) {
        resultContainer.style.display = 'flex';
    }
}

const showExceededText = () => {
    var resultContainer = document.getElementById('dg_widget_result_container');
    var target = document.getElementById('dg_widget_result_exceeded');
    target.style.display = 'block';
    if (resultContainer.childNodes.length) {
        resultContainer.style.display = 'flex';
    }
}

export { hideResults, clearResults, showResults, showEmptyText, showExceededText };