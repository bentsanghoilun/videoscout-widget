
const createIFrame = async (id) => {
    var embedTarget = document.getElementById(id);
    if (!id || !embedTarget) {
        console.error('Video Scout > Error: Invalid embed target');
        return false;
    }

    var styleElement = document.createElement("style");
    var iframeCss = `
        .dg_widget_searchInput{
            box-sizing: border-box;
        }
        .dg_widget_container {
            background-color: #fff;
            border-radius: 6px;
            box-shadow: 0 0.5em 1em -0.125em rgb(10 10 10 / 10%), 0 0 0 1px rgb(10 10 10 / 2%);
            color: #4a4a4a;
            display: block;
            padding: 1.25rem;
            display: block !important;
            margin: 12px;
        }
        .dg_widget_content{
            position: relative;
        }
        .dg_widget_searchInput{
            border-radius: 3px;
        }
        .dg_widget_result_container{
            width: 100%;
            height: auto !important;
            overflow-y: scroll;
            display: none;
            flex-direction: column;
            position: absolute;
            z-index: 999;
            background: #fff;
            border: 0px;
            top: 47px;
            max-height: 50vh;
            box-shadow: 0 0 1em rgb(10 10 10 / 10%);
            box-sizing: border-box;
        }
        .dg_widget_result_noResult_text{
            font-size: 14px;
        }
    `;
    styleElement.appendChild(document.createTextNode(iframeCss));
    document.body.append(styleElement);

    var content = `
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/discover-go-widget/discover-go-widget.css">
    <style>
    
    </style>
    <div class='dg_widget_container' style='display: none;'>
        <div class='dg_widget_content'>
            <div class='dg_widget_searchbar'>
                <span class="dg_widget_searchbar_icon"><i class='fa fa-search'></i></span>
                <input id='dg_widget_searchInput' class='dg_widget_searchInput' placeholder='Search for content' data-endpoint='search'/>
            </div>
            <div id='dg_widget_result_container' class='dg_widget_result_container'>
                <i class="fa fa-circle-o-notch fa-spin dg_widget_spinner dg_widget_hideable" style='display: none;'></i>
                <div class='dg_widget_result_noResult dg_widget_hideable' id='dg_widget_result_noResult' style='display: none;'>
                    <p class='dg_widget_result_noResult_text'><i class='fa fa-warning'></i> No Result Found!</p>
                </div>
                <div class='dg_widget_result_noResult dg_widget_hideable' id='dg_widget_result_shortNeedle' style='display: none;'>
                    <p class='dg_widget_result_noResult_text'><i class='fa fa-warning'></i> Please enter 3 or more characters to search</p>
                </div>
                <div class='dg_widget_result_noResult dg_widget_hideable' id='dg_widget_result_exceeded' style='display: none;'>
                    <p class='dg_widget_result_noResult_text'><i class='fa fa-warning'></i> Search limited to 50 characters.</p>
                </div>
            </div>
        </div>
    </div>
    `;
    embedTarget.insertAdjacentHTML('beforeend', content);

    return true;
}

export default createIFrame;