import doSearch from "./doSearch";
import { hideResults, clearResults, showResults } from "./resultsHandlers";

const searchListener = async (settings) => {
    const searchInput = document.getElementById('dg_widget_searchInput');
    var timeouter;

    searchInput.addEventListener('keyup', async (e) => { 
        clearTimeout(timeouter);
        timeouter = setTimeout(async () => {
            let needle = e.target.value.trim();
            const needleNoSpace = needle.replaceAll(' ', '');
            const hideables = document.querySelectorAll('.dg_widget_hideable');
            hideables.forEach(item => item.style.display = 'none');

            if (needleNoSpace.length === 0) {
                clearResults();
                return
            };

            if (needleNoSpace.length < 3) {
                document.getElementById('dg_widget_result_shortNeedle').style.display = 'block';
                return;
            }
            if (needle !== window.lastNeedle) { window.lastNeedle = needle };

            if (needle.includes(' ')) {
                needle = needle.split(' ').join('+');
            }
            needle = needle.toLowerCase();
            const searched = await doSearch(needle, settings.projectId, settings.token);
        }, 800);
    });

    document.addEventListener('click', (e) => {
        const isContent = e.target.closest('.dg_widget_container');
        if (!isContent) {
            hideResults();
        }
    })

    searchInput.addEventListener('focusin', () => {
        showResults();
    })

    return true;
}

export default searchListener;