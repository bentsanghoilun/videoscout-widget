import 'regenerator-runtime/runtime';
import createIFrame from './components/createIFrame';
import searchListener from './components/searchListener';

const embed = async (element) => {

    const settings = {
        projectId: element.dataset.projectid,
        token: element.dataset.token,
        embedTo: element.id
    }
    const componentCreated = await createIFrame(settings.embedTo);
    if (!componentCreated) { return };
    const addSearchListener = await searchListener(settings);
}
window.addEventListener('load', () => {
    var element = document.getElementById('videoscout_search');
    embed(element);
});